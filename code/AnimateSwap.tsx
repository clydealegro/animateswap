import * as React from "react"
import { useState, useEffect } from "react"
import { Frame, Stack } from "framer"
import { shuffle } from "lodash"

const spring = {
    type: "spring",
    damping: 20,
    stiffness: 300,
}

export function AnimateSwap() {
    const [colors, setColors] = useState(initialColors)

    useEffect(() => {
        setTimeout(() => setColors(shuffle(colors)), 1000)
    }, [colors])

    return (
        <Stack direction="horizontal">
            {colors.map(background => (
                <Frame
                    key={background}
                    size={140}
                    layoutTransition={spring}
                    radius={10}
                    style={{ background }}
                />
            ))}
        </Stack>
    )
}

const initialColors = ["#FF008C", "#D309E1", "#9C1AFF", "#7700FF"]
